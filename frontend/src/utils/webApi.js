import api from "./api";

export const getUser = async () => {
    return await api.get("/user");
}

export const getEmployees = async () => {
    return await api.get("/employees");
}

export const getExtracts = async () => {
    return await api.get("/extracts");
}
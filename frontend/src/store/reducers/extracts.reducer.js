//Type
export const ExtractsActionTypes = {
    ASYNC_ETRACTS: '@extracts/ASYNC_ETRACTS',

    CHANGE_EXTRACTS: '@extracts/CHANGE_EXTRACTS',
    CHANGE_SALE: '@extracts/CHANGE_SALE',
}

const INITIAL_STATE = {
    extracts: null,
    sale: null
};

export const extractsListReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case ExtractsActionTypes.CHANGE_EXTRACTS:
            return { ...state, extracts: payload };
        case ExtractsActionTypes.CHANGE_SALE:
            return { ...state, sale: payload };
        default:
            return state;
    }
};

export const getExtracts = () => ({
    type: ExtractsActionTypes.ASYNC_ETRACTS
});

export const changeExtracts = (extracts) => ({
    type: ExtractsActionTypes.CHANGE_EXTRACTS,
    payload: extracts
});

export const changeSale = (sale) => ({
    type: ExtractsActionTypes.CHANGE_SALE,
    payload: sale
});

export default extractsListReducer;
//Type
export const EmployeesActionTypes = {
    ASYNC_EMPLOYEES: '@employees/ASYNC_EMPLOYEES',

    CHANGE_EMPLOYEES: '@employees/CHANGE_EMPLOYEES',
}

const INITIAL_STATE = {
    employees: null,
};

export const employeesListReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case EmployeesActionTypes.CHANGE_EMPLOYEES:
            return { ...state, employees: payload };
        default:
            return state;
    }
};

export const getEmployees = () => ({
    type: EmployeesActionTypes.ASYNC_EMPLOYEES
});

export const changeEmployees = (employees) => ({
    type: EmployeesActionTypes.CHANGE_EMPLOYEES,
    payload: employees
});

export default employeesListReducer;
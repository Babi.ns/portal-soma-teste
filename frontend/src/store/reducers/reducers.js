import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router';

import User from "./user.reducer";
import Employees from "./employees.reducer";
import Extracts from "./extracts.reducer";

const RootReducer = (history) => combineReducers({
    router: connectRouter(history),
    User,
    Employees,
    Extracts
});

export default RootReducer;
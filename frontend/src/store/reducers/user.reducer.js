//Type
export const UsersActionTypes = {
    ASYNC_USER: '@user/ASYNC_USER',

    CHANGE_USER: '@user/CHANGE_USER',
}

const INITIAL_STATE = {
    user: null,
};

export const usersListReducer = (state = INITIAL_STATE, { type, payload }) => {
    switch (type) {
        case UsersActionTypes.CHANGE_USER:
            return { ...state, user: payload };
        default:
            return state;
    }
};

export const getUser = () => ({
    type: UsersActionTypes.ASYNC_USER
});

export const changeUser = (users) => ({
    type: UsersActionTypes.CHANGE_USER,
    payload: users
});

export default usersListReducer;
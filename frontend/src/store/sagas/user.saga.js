import { all, takeEvery, put, call } from 'redux-saga/effects';

import { UsersActionTypes, changeUser } from '../reducers/user.reducer';
import { getUser} from '../../utils/webApi';

function* user() {
    try {
        const response = yield call(getUser);
        yield put(changeUser(response.data));
    } catch (error) {
        console.log(error);
    }
}


export default function* MySaga() {
    yield all([
        yield takeEvery(UsersActionTypes.ASYNC_USER, user),
    ]);
}
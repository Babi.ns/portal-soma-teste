import { all } from 'redux-saga/effects';
import User from "./user.saga";
import Employees from "./employes.saga";
import Extracts from "./extracts.saga";

export default function* RootSaga() {
    yield all([
        User(),
        Employees(),
        Extracts()
    ]);
};
import { all, takeEvery, put, call } from 'redux-saga/effects';

import { EmployeesActionTypes, changeEmployees } from '../reducers/employees.reducer';
import { getEmployees} from '../../utils/webApi';

function* employees() {
    try {
        const response = yield call(getEmployees);
        yield put(changeEmployees(response.data));
    } catch (error) {
        console.log(error);
    }
}


export default function* MySaga() {
    yield all([
        yield takeEvery(EmployeesActionTypes.ASYNC_EMPLOYEES, employees),
    ]);
}
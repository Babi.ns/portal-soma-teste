import { all, takeEvery, put, call } from 'redux-saga/effects';

import { ExtractsActionTypes, changeExtracts, changeSale } from '../reducers/extracts.reducer';
import { getExtracts} from '../../utils/webApi';

function* extracts() {
    try {
        const response = yield call(getExtracts);
        yield put(changeSale(response.data.sale));
        yield put(changeExtracts(response.data.extracts));
    } catch (error) {
        console.log(error);
    }
}


export default function* MySaga() {
    yield all([
        yield takeEvery(ExtractsActionTypes.ASYNC_ETRACTS, extracts),
    ]);
}
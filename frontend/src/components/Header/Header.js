import React from 'react';
import HeaderView from './HeaderView';

function Header({userName}) {
  return (
    <HeaderView userName={userName} />
  );
}

export default Header;

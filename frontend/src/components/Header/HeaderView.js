import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { ContainerHeader, ContentHeader, TitleHeader, TextHeader } from './style/style';
import Button from '../Button/Button';
import { routesConsts } from '../../routes/routers.consts';

function HeaderView({userName}) {
  return (
    <ContainerHeader>
        <TitleHeader>Portal Empresa - Soma</TitleHeader>

        <ContentHeader>
            <Link to={routesConsts.EXTRACTS}>
              <Button text={"Extrato"}/>
            </Link>
            <Link to={routesConsts.ROOT}>
              <Button text={"Funcionários"} />
            </Link>
        </ContentHeader>

        <ContentHeader>
            <TextHeader>Login:</TextHeader>
            <TextHeader>{userName}</TextHeader>
        </ContentHeader>
    </ContainerHeader>
  );
}

export default HeaderView;

HeaderView.propTypes = {
  userName: PropTypes.string
}
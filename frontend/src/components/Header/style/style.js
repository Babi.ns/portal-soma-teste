import styled from "styled-components";

export const ContainerHeader = styled.section`
  width: 100%;
  padding: 10px 20px;
  box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.2);

  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const ContentHeader = styled.section`
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const TitleHeader = styled.span`
    font-family: titillium-web;
    font-size: 18px;
    font-weight: 600;
    color: #5c2483;
`;

export const TextHeader = styled.span`
    font-family: titillium-web;
    font-size: 16px;
    color: #e84e0f;
    padding-right: 3px;

    &:first-child {
        font-weight: bold;
    }
`;
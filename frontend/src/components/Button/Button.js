import React from 'react';
import PropTypes from 'prop-types';

import { ButtonView, ContentButton } from './style/style';

function Button({text, onClick}) {
  return (
    <ContentButton>
      <ButtonView onClick={onClick}>
        {text}
      </ButtonView>
    </ContentButton>
  );
}

export default Button;

Button.propTypes = {
  onClick: PropTypes.func,
  text: PropTypes.string,
};
import styled from "styled-components";

export const ContentButton = styled.section`
    display: flex;
    justify-content: center;
    align-items: center;
    align-content: center;

    padding: 6px;
`;

export const ButtonView = styled.button`
    width: 120px;
    height: 36px;

    font-family: titillium-web;
    font-weight: bold;
    font-size: 16px;

    background-color: #fff;
    color:  #5c2483;
    border-radius: 8px;
    border: 1px solid  #5c2483;
    
    cursor: pointer;

    &:hover {
        background-color:  #5c2483;
        color: #fff;
        box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.4);
    }
`;
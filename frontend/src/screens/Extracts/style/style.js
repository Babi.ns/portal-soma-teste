import styled from "styled-components";

export const ContainerExtracts = styled.section`
    width: 100%;
    padding: 10px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
`;

export const ContentHeader = styled.section`
    width: 100%;
    display: flex;
    justify-content: space-between;
    align-items: center;
`;

export const ContentTitle = styled.section`
    display: flex;
    justify-content: center;
    align-items: center;
`;

export const TitleSale = styled.span`
    font-family: titillium-web;
    font-size: 20px;
    font-weight: bold;
    color: #5c2483;

    &:last-child{
        color: green;
        margin-left: 4px;
    }
`;
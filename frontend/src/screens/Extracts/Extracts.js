import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getExtracts } from '../../store/reducers/extracts.reducer';
import ExtractsView from './ExtractsView';
import { columns } from './columnsTable';

function Extracts() {
  const dispatch = useDispatch();
  const extracts = useSelector(state => state.Extracts.extracts);
  const sale = useSelector(state => state.Extracts.sale);

  useEffect(() => {
    dispatch(getExtracts());
    // eslint-disable-next-line
  },[]);

  return (
    <ExtractsView 
      Data={extracts}
      Columns={columns}
      keyName={"id"}
      valueSale={sale && sale}
    />
  );
}

export default Extracts;

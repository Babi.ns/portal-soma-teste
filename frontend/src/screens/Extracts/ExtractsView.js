import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

import {ContainerExtracts, ContentHeader, ContentTitle, TitleSale} from "./style/style";
import { TitleScreen } from '../../styles/styles';

function ExtractsViews({Data, Columns, keyName, valueSale}) {
  return (
    <ContainerExtracts>
      <ContentHeader>
        <TitleScreen>Extratos</TitleScreen>
        <ContentTitle>
          <TitleSale>Saldo:</TitleSale>
          <TitleSale>{valueSale}</TitleSale>
        </ContentTitle>
      </ContentHeader>
      <Table
          keyName={"id"}
          className="TableAntd"
          bordered
          rowKey={keyName}
          dataSource={Data}
          columns={Columns}
          size="middle"
          pagination={false}
      />
    </ContainerExtracts>
  );
}

export default ExtractsViews;

ExtractsViews.propTypes = {
  Data: PropTypes.array,
  Columns: PropTypes.array,
  keyName: PropTypes.string,
  valueSale: PropTypes.string
};

export const columns = [
    {
      title: 'Data Operação',
      dataIndex: 'date_operation',
    },
    {
      title: 'Tipo Operação',
      dataIndex: 'type_operation',
    },
    {
      title: 'Valor Operação',
      dataIndex: 'value_operation',
    },
  ];
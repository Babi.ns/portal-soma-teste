import React from 'react';
import PropTypes from 'prop-types';
import { Table } from 'antd';

import {ContainerEmprloyees} from "./style/style";
import { TitleScreen } from '../../styles/styles';

function EmployeesView({Data, Columns, keyName}) {
  return (
    <ContainerEmprloyees>
        <TitleScreen>Funcionários</TitleScreen>
        <Table
            keyName={"id"}
            className="TableAntd"
            bordered
            rowKey={keyName}
            dataSource={Data}
            columns={Columns}
            size="middle"
            pagination={false}
        />
    </ContainerEmprloyees>
  );
}

export default EmployeesView;

EmployeesView.propTypes = {
  Data: PropTypes.array,
  Columns: PropTypes.array,
  keyName: PropTypes.string
};

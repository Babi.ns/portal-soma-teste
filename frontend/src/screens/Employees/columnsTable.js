export const columns = [
    {
      title: 'Matrícula',
      dataIndex: 'registration',
    },
    {
      title: 'Nome',
      dataIndex: 'name',
    },
    {
      title: 'Função',
      dataIndex: 'occupation',
    },
  ];
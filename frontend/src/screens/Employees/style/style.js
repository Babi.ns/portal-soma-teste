import styled from "styled-components";

export const ContainerEmprloyees = styled.section`
  width: 100%;
  padding: 10px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: flex-start;
`;
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

import { getEmployees } from '../../store/reducers/employees.reducer';
import EmployeesView from './EmployeesView';
import { columns } from './columnsTable';

function Employees() {
  const dispatch = useDispatch();
  const employees = useSelector(state => state.Employees.employees);

  useEffect(() => {
    dispatch(getEmployees());
    // eslint-disable-next-line
  }, []);
  
  return (
    <EmployeesView 
      Data={employees}
      Columns={columns}
      keyName={"id"}
    />
  );
}

export default Employees;

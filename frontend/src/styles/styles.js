import styled from "styled-components";

export const Container = styled.div`
  width: 100%;
  height: 100%;
`;

export const Content = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 4px;
  padding: 20px;
`;

export const TitleScreen = styled.span`
  font-family: titillium-web;
  font-size: 24px;
  font-weight: bold;
  color: #5c2483;
`;

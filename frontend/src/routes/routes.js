import React, {Suspense, lazy} from "react";
import { Route, Switch } from "react-router-dom";

import {routesConsts} from "./routers.consts";
const Employees = lazy(() => import("../screens/Employees/Employees"));
const Extracts = lazy(() => import("../screens/Extracts/Extracts"));

export default function Routes() {
  return (
    <Suspense fallback={"Carregando..."}>
      <Switch>
        <Route path={routesConsts.ROOT} exact component={Employees} />
        <Route path={routesConsts.EXTRACTS} exact component={Extracts} />
      </Switch>
    </Suspense>
  );
}

import React, { useEffect } from 'react';

import Routes from './routes/routes';
import { BrowserRouter } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';

import Header from './components/Header/Header';

import { Container, Content } from './styles/styles';
import GlobalStyle  from './styles/Global';
import 'antd/dist/antd.css';
import './styles/customTable.css';
import { getUser } from './store/reducers/user.reducer';

function App() {
  const dispatch = useDispatch();
  const user = useSelector(state => state.User.user);

  useEffect(() => {
    dispatch(getUser());
    // eslint-disable-next-line
  },[]);

  return (
    <BrowserRouter>
      <Container>
        <Header userName={user && user.name} />
        <Content>
          <Routes/>
        </Content>
        <GlobalStyle  />
      </Container>
    </BrowserRouter>
  );
}

export default App;


module.exports = {
    async getUser(req, res){
        const user = {
            id: 2019,
            name:  "Luiz Dalcio"
        }
        return res.status(200).send(user);
    },
    async getEmployees(req, res){
        const employess = [
            {
                id: 1,
                registration: 1001,
                name: "Luiz Dalcio",
                occupation: "Analista"
              },
              {
                id: 2,
                registration: 1002,
                name: "Pedro Ribeiro",
                occupation: "Administrativo"
              },
              {
                id: 3,
                registration: 1003,
                name: "João Marcos",
                occupation: "Financeiro"
              }
        ];

        return res.status(200).send(employess);
    },
    async getExtracts(req, res){
      const sale = "R$ 45.600,00";
      const extracts = [
          {
              id: 1,
              date_operation: "23/11/2019",
              type_operation: "Saque ATM",
              value_operation: "- R$ 450,00"
            },
            {
              id: 2,
              date_operation: "24/11/2019",
              type_operation: "Saque ATM",
              value_operation: "- R$ 450,00"
            },
            {
              id: 3,
              date_operation: "25/11/2019",
              type_operation: "Saque ATM",
              value_operation: "- R$ 450,00"
            },
            {
              id: 4,
              date_operation: "26/11/2019",
              type_operation: "Pagamento de folha",
              value_operation: "- R$ 50.200,00"
            },
            {
              id: 5,
              date_operation: "26/11/2019",
              type_operation: "Vendas TEF",
              value_operation: "R$ 100.700,00"
            }
      ];

      return res.status(200).send({sale, extracts});
    }
};
const express = require('express');
const UserController = require('./controllers/UserController');

const routes = express.Router();

routes.get('/user', UserController.getUser);
routes.get('/employees', UserController.getEmployees);
routes.get('/extracts', UserController.getExtracts);

module.exports = routes;